# react-test

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/react-test.svg)](https://www.npmjs.com/package/react-test) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save react-test
```

## Usage

```jsx
import React, { Component } from 'react'

import MyComponent from 'react-test'
import 'react-test/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

## License

MIT © [melvin.solomon](https://github.com/melvin.solomon)
